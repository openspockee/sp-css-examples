![Spockee Live Video Shopping](./images/spockee.png "Spockee Live Video Shopping")

# Customize your Shopping Parties

## Introduction
You can find examples of custom implementations to display your Shopping-Parties :

- **Example 1 – default styles** : Display the tiles with de default spockee styles 

- **Example 2 – grid layout** : Display the tiles with a grid layout and hover effects 

- **Example 3 - carousel** : Display the tiles with a Carousel once the `spockeeLoaded` event is fired 


----
## Use your own Shopping Parties

To have a better idea of the presentation for your Shopping Parties, you can simply change the `storeId` inside the `index.html` file with yours provided by Spockee :

```html
<div id="ID_OF_THE_CONTAINER" style="border-bottom: solid 1px black"></div> 
<script> 
  (function () { 
    var spck = { 
      storeId: 'YOUR_ID', // given by Spockee 
      storeType: "YOUR_STORE_TYPE", // given by Spockee 
      customColor: 'YOUR_MAIN_COLOUR', // Main colour of the tiles and Shopping Party player 
      containerId: 'ID_OF_THE_CONTAINER', // id of the container to append the  list of your Shopping Parties 
      keywords: [], // array of keywords to filter your broadcast 
      displayType: 'all', // "all" | "upcoming" | "vod" | "one" 
      staging: true, // false in production environment | true in staging      environment 
      refreshOnClose: true, // false will prevent the refresh of the tag when the user closes the Shopping Party 
      useDefaultStyles: false, // true if you want to use your custom styles 
      broadcastId: "ID_OF_BROADCAST" // only if displayType : "one" 
    }; 

    var el = document.createElement('script'); 
    el.setAttribute('src', 'https://party.spockee.io/builder/' + spck.storeId); 
    el.setAttribute('data-spck', JSON.stringify(spck)); 
    document.body.appendChild(el); 
  })(); 
</script> 
```


---
## Customization guide for developers

For more details, you can download the customization guide for developers inside this repository.

---
## Website 
https://www.spockee.com